import os
import gc
import pandas as pd
import numpy as np
import tensorflow as tf
from sklearn.preprocessing import LabelEncoder, MinMaxScaler, StandardScaler
from sklearn.metrics import roc_auc_score, roc_curve, precision_recall_curve, confusion_matrix
from sklearn.model_selection import train_test_split
# read training csv
train_df = pd.read_csv('train.csv')
#drop some columns
#dropcol = ['PassengerId','Name','Sex','Ticket','Cabin','Embarked']

dropcol = ['PassengerId','Name','Age','Sex','Ticket','Cabin','Embarked']
train_df.drop(dropcol, axis=1, inplace=True)
#take class coulmn
labels = train_df.pop('Survived')
#merged_df.fillna(0, inplace=True)

# Reshape (one-hot)
target = np.zeros([len(labels), len(np.unique(labels))])
target[:, 0] = labels == 0
target[:, 1] = labels == 1

#take only value; remove column header
train_df = train_df.values

# Create a validation set to check training performance
X_train, X_valid, y_train, y_valid = train_test_split(train_df, target, test_size=0.1, random_state=2, stratify=target[:, 0])

# # Graph
tf.reset_default_graph()
sess = tf.InteractiveSession()

# Define placeholders for the 4 input varaibles and 2 output varaibles\
X_cont = tf.placeholder(tf.float32, shape=(None, 4), name='X_cont')
y = tf.placeholder(tf.int32, shape=(None, 2), name='labels')
train_mode = tf.placeholder(tf.bool)


# Add embeddings to input
X = tf.identity(X_cont)

#build a simple graph with 1 hidden layer

hidden_layer_1 = tf.layers.dense(inputs=X,
                                 units=80,
                                 name='first_hidden_layer',
                                 kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=0.3))
hidden_layer_1 = tf.layers.batch_normalization(hidden_layer_1, training=train_mode)
hidden_layer_1 = tf.nn.relu(hidden_layer_1)
logits = tf.layers.dense(inputs=hidden_layer_1, units=2, name='outputs')

# Define the loss function for training as cross entropy

xent = tf.nn.softmax_cross_entropy_with_logits_v2(labels=y, logits=logits)
loss = tf.reduce_mean(xent, name='loss')

# Define the optimiser

optimiser = tf.train.AdamOptimizer()  # AdagradOptimizer(learning_rate=LEARNING_RATE)
train_step = optimiser.minimize(loss)

# Output the class probabilities to I can get the AUC

predict = tf.argmax(logits, axis=1, name='class_predictions')
predict_proba = tf.nn.softmax(logits, name='probability_predictions')


# Initialisation node and saver
init = tf.global_variables_initializer()
#saver = tf.train.Saver()



tf.summary.scalar("loss", loss)
#tf.summary.scalar("eval", predict_proba)
merged_summary_op = tf.summary.merge_all()


#block for start training

with tf.Session() as sess:
    summary_writer = tf.summary.FileWriter("./log", graph=tf.get_default_graph())
    init.run()
    # Begin epoch loop
    print('Training for 400 iterations over 10 epochs with batchsize full...')
    counter=0
    for epoch in range(10):

        for iteration in range(400):
            counter=counter+1
            #sending all data in each batch
#            merge = tf.summary.merge_all()
            extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            summary,_,_=sess.run([merged_summary_op,train_step,extra_update_ops], feed_dict= {X_cont:X_train, y:y_train,train_mode: 1})
            summary_writer.add_summary(summary, counter)
        #for each epoch check training and validation progress.
        y_pred_train, y_prob_train = sess.run([predict, predict_proba], feed_dict={X_cont:X_train, y:y_train,train_mode: 0})
        tauc=roc_auc_score(y_train[:, 1], y_prob_train[:, 1])

        y_pred_val, y_prob_val = sess.run([predict, predict_proba], feed_dict={X_cont:X_valid, y:y_valid,train_mode: 0})
        vauc=roc_auc_score(y_valid[:, 1], y_prob_val[:, 1])
        #print  info
        print('Epoch = {}, Train AUC = {:.8f}, Valid AUC = {:.8f}' .format(epoch, tauc,vauc))

#[3,1,0,7.7500] [0,1]


    print('Training complete.')
    #predict a new sample [  3  ,    1,      0,    7.7500]
    y_prob  = sess.run(predict_proba, feed_dict={X_cont:[[3,1,0,7.7500],[3,1,0,7.10]], train_mode: 0})
    print(y_prob[:,1])
