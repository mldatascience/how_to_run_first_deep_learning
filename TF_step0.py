import tensorflow as tf
#x = tf.constant(4)
#y = tf.constant(10)
#z = tf.add(x, y)
#sess = tf.Session()
#print (sess.run(z))




x = tf.placeholder(tf.float32, shape=[2, 2])
y = tf.constant([[1.0, 1.0], [0.0, 1.0]])
m = tf.matmul(x, y)
a = tf.add(x, y)

sess = tf.Session()
A,M=sess.run([a,m], feed_dict={x:[[2,3],[4,5]]})
print(A)
print(M)
summary_writer=tf.summary.FileWriter('./log/training', graph=tf.get_default_graph())
