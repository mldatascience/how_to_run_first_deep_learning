import os
import gc
import pandas as pd
import numpy as np
import tensorflow as tf
from sklearn.preprocessing import LabelEncoder, MinMaxScaler, StandardScaler
from sklearn.metrics import roc_auc_score, roc_curve, precision_recall_curve, confusion_matrix
from sklearn.model_selection import train_test_split
# read training csv
train_df = pd.read_csv('train.csv')




#drop some columns
dropcol = ['PassengerId','Name','Age','Sex','Ticket','Cabin','Embarked']
train_df.drop(dropcol, axis=1, inplace=True)
#take class coulmn
labels = train_df.pop('Survived')



#merged_df.fillna(0, inplace=True)

# Reshape (one-hot)
target = np.zeros([len(labels), len(np.unique(labels))])
target[:, 0] = labels == 0
target[:, 1] = labels == 1

#take only value; remove column header
train_df = train_df.values

# Create a validation set to check training performance
X_train, X_valid, y_train, y_valid = train_test_split(train_df, target,
test_size=0.1, random_state=2, stratify=target[:, 0])







# # Graph
tf.reset_default_graph()
tf.InteractiveSession()
# Define placeholders for the 4 input varaibles and 2 output varaibles\
with tf.name_scope("inputs"):
    X = tf.placeholder(tf.float32, shape=(None, 4), name='X')
    y = tf.placeholder(tf.int32, shape=(None, 2), name='labels')
    train_mode = tf.placeholder(tf.bool)

X = tf.identity(X)

with tf.name_scope("layers"):
#build a simple graph with 1 hidden layer
    hidden_layer_1 = tf.layers.dense(inputs=X,units=80,
                    name='first_hidden_layer',
                    kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=0.3))
    hidden_layer_1 = tf.layers.batch_normalization(hidden_layer_1, training=train_mode)
    hidden_layer_1 = tf.nn.relu(hidden_layer_1)


    drop_layer_1 = tf.layers.dropout(inputs=hidden_layer_1,
                                         rate=0.4,
                                         name='first_dropout_layer',
                                         training=train_mode)

    hidden_layer_2 = tf.layers.dense(inputs=drop_layer_1,
                    units=40,
                    name='second_hidden_layer',
                    kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=0.1))
    hidden_layer_2 = tf.layers.batch_normalization(hidden_layer_2, training=train_mode)
    hidden_layer_2 = tf.nn.relu(hidden_layer_2)



with tf.name_scope("outputs"):
    output = tf.layers.dense(inputs=hidden_layer_2, units=2, name='outputs')

    # Define the loss function for training as cross entropy
    xent = tf.nn.softmax_cross_entropy_with_logits_v2(labels=y, logits=output)
    loss = tf.reduce_mean(xent, name='loss')

    # Define the optimiser
    optimiser = tf.train.AdamOptimizer()
    train_step = optimiser.minimize(loss)

    # Output the class probabilities to I can get the AUC
    predict = tf.argmax(output, axis=1, name='class_predictions')
    predict_proba = tf.nn.softmax(output, name='probability_predictions')



with tf.name_scope('accuracy'):
  with tf.name_scope('correct_prediction'):
    correct_prediction = tf.equal(predict, tf.cast(y[:,1], tf.int64))
  with tf.name_scope('accuracy'):
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


tf.summary.scalar('accuracy', accuracy)
tf.summary.scalar("loss", loss)

merged_summary_op = tf.summary.merge_all()

# Initialisation node and saver
init = tf.global_variables_initializer()


BATCH_SIZE=800
summary_writer_acc=tf.summary.FileWriter('./log/validation', graph=tf.get_default_graph())
summary_writer=tf.summary.FileWriter('./log/training', graph=tf.get_default_graph())
sess=tf.Session()
sess.run(init)
counter=1
for epoch in range(10):
    for iteration in range(400):
        idx = np.random.choice(np.where(y_train)[0], size= BATCH_SIZE )
        extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        summary,_,_=sess.run([ merged_summary_op,train_step,extra_update_ops], feed_dict= {X:X_train, y:y_train,train_mode: 1})
        summary_writer.add_summary(summary, counter)

        summary,_,_=sess.run([merged_summary_op,predict, predict_proba],feed_dict={X:X_valid, y:y_valid,train_mode: 0})
        summary_writer_acc.add_summary(summary, counter)

        counter=counter+1
    summary,y_pred_train, y_prob_train = sess.run([merged_summary_op,predict, predict_proba],
                                feed_dict={X:X_train, y:y_train,train_mode: 0})
    tauc=roc_auc_score(y_train[:, 1], y_prob_train[:, 1])

    y_pred_val, y_prob_val = sess.run([predict, predict_proba],
                                feed_dict={X:X_valid, y:y_valid,train_mode: 0})
    vauc=roc_auc_score(y_valid[:, 1], y_prob_val[:, 1])
 
    print('Epoch = {}, Train AUC = {:.8f}, Valid AUC = {:.8f}' .format(epoch, tauc,vauc))
